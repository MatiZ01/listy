// Delete first element of given name

void pop_by_surname(struct Client **head, char *p_surname)
{		
    struct Client *wsk = *head;
    
    // if *head is element to delete
    if(strcmp(wsk -> surname, p_surname) == 0) {
        struct Client *deleted = wsk; //remember the deleted element
        head = &(deleted -> next); // move the pointer to skip the deleted element 
        free(deleted); //delete from memory
        return;
    }

    while(wsk -> next != NULL) //wsk points to not last element
    {
        if(strcmp(wsk -> next -> surname, p_surname) == 0) //we need to have a pointer to the preceding element
        {
            struct Client *deleted = wsk -> next; //remember the deleted element
            wsk -> next = deleted -> next; // move the pointer to skip the deleted element 
            free(deleted); //delete from memory
            return;
        }
        else 
        {
            wsk = wsk -> next; //if it has not been removed, move on
        }
    }

	return;
}