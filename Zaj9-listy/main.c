#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "moduly.h"

int main()
{
    int choice = 0;
    char surname[20] = "";

    struct Client *head; //Head of the list definition
    head = (struct Client*)malloc(sizeof(struct Client)); //Which doesn't work :( edit. Name of the struct must be the same as declared :)
    strcpy(head->surname, "Smith"); //Assigning some surname 
    head->next = NULL;


    printf("In this program there are following options:\n" //Options
    "[1] Adding a new client to the end of the list.\n"
    "[2] Deleting a client of given surname.\n"
    "[3] Displaying the number of clents.\n"
    "[4] Displaying surnames of clients.\n"
    "What would you like to do?\n");

    scanf("%d", &choice); //Choosing from options
    if (choice < 1 || choice > 4) //Repeating if wrong number 
    {
        do
        {
            printf("Wrong number. Try again");
            scanf("%d", &choice);
        } while (choice < 1 || choice > 4);
        
    }
    else //If everything's good, doing some stuff
    {
        switch(choice)
        {
            case 1: //Adding a new client to the end of the list
                printf("Enter a new surname: ");
                scanf("%s", surname);
                push_back(&head, surname);
                //show_list(head); //checking if worked
            break;

            case 2: //Delete the first element of given name
                printf("Enter a new surname: ");
                scanf("%s", surname);
                pop_by_surname(&head, surname);
                //show_list(head); //same here
            break;

            case 3: //Number of clients 
                printf(list_size(head));
            break;

            case 4: //Clients' surnames
                show_list(head);
            break;
            
            default:
                printf("Wrong number");
            break;
        }
    }
    return 0;
}
